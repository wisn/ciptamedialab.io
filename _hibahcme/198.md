---
nomor: 198
nama: Saskia Prisandhini Tjokro
foto:
  filename: 20170620_092743-1 (1).jpg
  type: image/jpeg
  size: 1463223
  url: >-
    http://5c4cf848f6454dc02ec8-c49fe7e7355d384845270f4a7a0a7aa1.r53.cf2.rackcdn.com/3a2f976f-f682-43b5-a35d-a0426be62e22/20170620_092743-1%20(1).jpg
seni: sastra
pengalaman: >-
  13 tahun (5 tahun pasif berkarya karena hamil, bekerja dan mengurus anak sejak
  2011 - 2015)
website: ''
sosmed: ''
file:
  filename: >-
    Contoh Karya (Sebuah Dongeng Untuk Masa Lalu dan Babad Kebumian - Prosa
    Pendek).pdf
  type: application/pdf
  size: 172585
  url: >-
    http://5c4cf848f6454dc02ec8-c49fe7e7355d384845270f4a7a0a7aa1.r53.cf2.rackcdn.com/8bcf2eed-875c-462a-86ce-8b45f85da70d/Contoh%20Karya%20(Sebuah%20Dongeng%20Untuk%20Masa%20Lalu%20dan%20Babad%20Kebumian%20-%20Prosa%20Pendek).pdf
proyek: Roman Hikayat Babad Kebumian
lokasi: 'Jawa Tengah, Kebumen'
lokasi_id: Q3557
deskripsi: "Kebumen merupakan daerah di jalur pantai selatan Pulau Jawa, dikenal sebagai situs geologis dan gua-gua tepi laut. Secara budaya, Kebumen meleburkan diri pada Banyumasan, Keraton Jawa Tengah, Kejawen, dan Tionghoa (Gombongan). Proyek ini merupakan proyek pembuatan sebuah roman hikayat kontemporer berdasarkan legenda-legenda dan cerita rakyat di sana, dengan keluaran sebuah buku roman hikayat. \r\n\r\nRencananya, naskah ini akan diberi judul \"Babad Kebumian\" yang menitikberatkan keunikan saling-silang budaya dan geografis Kebumen. Tak hanya ingin menceritakan ulang cerita-cerita lokal yang terdata, saya ingin merangkainya menjadi suatu kesatuan roman yang dapat menjadi kebanggaan, syukur-syukur menjadi seperti Mahabharata asli Indonesia. Saat ini saya sudah menulis konsep dasar Babad Kebumian berbentuk dongeng (saya lampirkan bersama lampiran contoh karya). Inspirasinya didasari oleh 2 kunjungan singkat swadaya ke Kebumen 2016 -  2017. \r\n\r\nUntuk menulis buku ini, saya membutuhkan perjalanan ke Kebumen dskt. untuk bertemu narasumber, menjelajah kondisi alamnya, dan merekam cerita rakyat Kebumen lebih dalam. Saya memerlukan bantuan tenaga ahli sosiolog / antropolog untuk membantu memberi perspektif obyektif terhadap perekaman budaya lokal. \r\n\r\nProyek ini membutuhkan 9 bulan, dengan linimasa 2 bulan pertama pemetaan mendalam hikayat lokal, 2 bulan kedua kroscek data sepanjang jalur pantai selatan Jawa; lalu dilanjutkan dengan penulisan naskah, pembuatan dummy serta sosialisasi."
kategori: perjalanan
latar_belakang: "Semua diawali dari perjalanan saya ke Kebumen tahun 2016. Saat itu saya berjumpa dengan forum Silaturahmi Komunitas Muda Kebumen yang anggotanya beragam. Dari seniman,pengrajin, pecinta binatang, penggiat pariwisata, museum lokal, penganut kepercayaan, penjelajah gua, sampai petani lokal. Berangkat dari sana, saya menjelajah dan menyadari keunikan Kebumen yang budayanya bersaling-silang antara Keraton, Banyumasan, Kejawen, dan Tionghoa. Kebumen sendiri merupakan kota tua perdagangan dan industri, memiliki Pecinan sejak era kolonial. Sementara itu, kondisi geografisnya indah, persawahan, bukit dan hutan berlatar gua-gua mistik dan pantai selatan Jawa. Tergugah, saya menulis dongeng pendek berjudul \"Babad Kebumian\". \r\n\r\nTahun 2017, saya kembali ke Kebumen dan menyebar dongeng tersebut. Reaksinya hangat. Sekelompok penari dan penembang wayang berniat menginterpretasi dongeng itu menjadi karya. Beberapa tokoh mengundang saya untuk mendengar cerita mereka lebih dalam. Seketika, ide menarik bermunculan. Betapa saya ingin menuliskan Babad Kebumian ini menjadi sebuah roman hikayat!\r\n\r\nSaya sadar, penulisan roman berdasarkan hikayat membutuhkan pendalaman supaya tidak mengadaptasi 'kulit'nya saja; seperti yang terjadi pada interpretasi populer Nyi Roro Kidul di televisi (dari sosok seorang dewi menjadi hantu gentayangan haus lelaki). Untuk itu saya harus meneliti lebih detail Kebumen dan jalur pantai selatan. Butuh waktu, konsentrasi, dan dana. Inilah alasan saya mendaftar hibah ini."
masalah: "Saya harap penulisan naskah saya ini dapat membantu mengangkat identitas budaya masyarakat Kebumen yang unik, sekaligus mengapresiasi animo masyarakat lokalnya yang sangat mendorong penulisan hikayat ini. Harapannya, roman hikayat ini dapat memperkaya khazanah sastra nusantara. Syukur-syukur dapat mengilhami interpretasi karya seni lain di masa depan.\r\n\r\nSecara teknis, dana hibah akan menyelesaikan masalah saya dalam hal:\r\n1. Biaya perjalanan (transportasi, akomodasi) selama pendataan latar belakang / hikayat budaya di Kebumen\r\n2. Penyediaan asistensi obyektif dari peneliti budaya selama perjalanan\r\n3. Penyediaan tenaga bantuan untuk menjaga anak saya di Tangerang selama saya meneliti di lapangan (Jawa Tengah)\r\n4. Bantuan biaya sosialisasi lokal selama penulisan, untuk mendorong rasa memiliki\r\n5. Bantuan biaya alat tulis kantor selama penulisan dan pembuatan dummy buku \r\n"
durasi: 9 bulan
sukses: "1. Tercatatnya data budaya lokal dan riwayat-riwayat di Kebumen dan sekitarnya\r\n2. Tertulisnya sebuah roman hikayat berjudul \"Babad Kebumian\"\r\n3. Terciptanya keselarasan isi \"Babad Kebumian\" dengan budaya lokal dan riwayat-riwayat di Kebumen\r\n4. Tanggapan Roman Babad Kebumian oleh masyarakat setempat (minimal 5 pernyataan tertulis / testimoni dari tokoh / pemangku kepentingan kunci setempat)\r\n5. Sosialisasi buku di Kebumen dan kota besar lain di Indonesia (di museum atau pusat-pusat kebudayaan)"
dana: '146'
submission_id: 5a79643bbcf7ae6d5aa94441
---
